#include "gen_voxel_from_pcd.h"
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <limits> 
//#include <sensor_msg/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>

GenVoxelFromPcd::voxel_grid GenVoxelFromPcd::genVoxelFromPcd(const std::string &pcd_file,
                                    const geometry_msgs::Pose &trans_camera_to_palm,
                                    const cv::Point3i &palm_voxel_dim,
                                    const cv::Point3f &palm_voxel_size)
{
    std::cout << pcd_file << std::endl;
    PointCloudXYZRGB::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::io::loadPCDFile(pcd_file, *cloud);
    // Transform the point cloud into palm pose frame.
    Eigen::Quaternionf rotation(trans_camera_to_palm.orientation.w, trans_camera_to_palm.orientation.x,
                                trans_camera_to_palm.orientation.y, trans_camera_to_palm.orientation.z);
    Eigen::Matrix<float, 3, 1> translation;
    translation(0, 0) = trans_camera_to_palm.position.x;
    translation(1, 0) = trans_camera_to_palm.position.y; 
    translation(2, 0) = trans_camera_to_palm.position.z;
    PointCloudXYZRGB::Ptr trans_cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    transformPointCloud(*cloud, *trans_cloud, translation, rotation);
    cloud->header.frame_id = "blensor_camera";
    trans_cloud->header.frame_id = "grasp_palm_pose";
    publishPointcloud(*cloud, "cloud", *trans_cloud, "cloud_palm");

    voxel_grid palm_voxel_grid;
    std::vector<std::vector<std::vector<int> > > palm_voxel_grid_bi(palm_voxel_dim.x, 
                                                std::vector<std::vector<int> >(palm_voxel_dim.y, 
                                                std::vector<int>(palm_voxel_dim.z, 0)));
    PointCloudXYZRGB::iterator iter_point;

    cv::Point3d palm_voxel_min_loc;
    palm_voxel_min_loc.x = 0.;
    palm_voxel_min_loc.y = - palm_voxel_dim.y / 2 * palm_voxel_size.y;
    palm_voxel_min_loc.z = - palm_voxel_dim.z / 2 * palm_voxel_size.z;

    for(iter_point = cloud->points.begin(); iter_point < cloud->points.end(); iter_point++)
    {
        if (std::isnan(iter_point->x) || std::isnan(iter_point->y) || std::isnan(iter_point->z))
            continue;
        cv::Point3i voxel_loc; 
        voxel_loc.x = int((iter_point->x - palm_voxel_min_loc.x) / palm_voxel_size.x);
        voxel_loc.y = int((iter_point->y - palm_voxel_min_loc.y) / palm_voxel_size.y);
        voxel_loc.z = int((iter_point->z - palm_voxel_min_loc.z) / palm_voxel_size.z);
        if(voxel_loc.x >= 0 && voxel_loc.x < palm_voxel_dim.x &&
           voxel_loc.y >= 0 && voxel_loc.y < palm_voxel_dim.y &&
           voxel_loc.z >= 0 && voxel_loc.z < palm_voxel_dim.z)
            if(palm_voxel_grid_bi[voxel_loc.x][voxel_loc.y][voxel_loc.z] != 1)
            {
                palm_voxel_grid_bi[voxel_loc.x][voxel_loc.y][voxel_loc.z] = 1;
                palm_voxel_grid.push_back(voxel_loc);
            }
    }
  
    return palm_voxel_grid; 
}

GenVoxelFromPcd::voxel_grid GenVoxelFromPcd::genVoxelFromPcd(const std::string &pcd_file,
                                    const std::vector<double> &trans_camera_to_palm,
                                    const cv::Point3i &palm_voxel_dim,
                                    const cv::Point3f &palm_voxel_size)
{
    bool palm_frame = false;
    std::cout << pcd_file << std::endl;
    PointCloudXYZRGB::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::io::loadPCDFile(pcd_file, *cloud);
    Eigen::Matrix4f transformation = Eigen::Matrix4f::Identity();
    for(int i = 0; i < 4; ++i)
        for(int j = 0; j < 4; ++j)
            transformation(i, j) = trans_camera_to_palm[4 * i + j];
    PointCloudXYZRGB::Ptr trans_cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    transformPointCloud(*cloud, *trans_cloud, transformation);
 
    cloud->header.frame_id = "blensor_camera";
    if(palm_frame)
    {
        trans_cloud->header.frame_id = "grasp_palm_pose";
        // Publish pointclouds for debugging
        publishPointcloud(*cloud, "cloud", *trans_cloud, "cloud_palm");
    }
    else
    {
        trans_cloud->header.frame_id = "object_pose";
        publishPointcloud(*cloud, "cloud", *trans_cloud, "cloud_object");
    }

    voxel_grid palm_voxel_grid;
    std::vector<std::vector<std::vector<int> > > palm_voxel_grid_bi(palm_voxel_dim.x, 
                                                std::vector<std::vector<int> >(palm_voxel_dim.y, 
                                                std::vector<int>(palm_voxel_dim.z, 0)));
    PointCloudXYZRGB::iterator iter_point;

    cv::Point3d palm_voxel_min_loc;
    if(palm_frame)
    {
        palm_voxel_min_loc.x = 0.;
        palm_voxel_min_loc.y = - palm_voxel_dim.y / 2 * palm_voxel_size.y;
        palm_voxel_min_loc.z = - palm_voxel_dim.z / 2 * palm_voxel_size.z;
    }
    else
    {
        palm_voxel_min_loc.x = - palm_voxel_dim.x / 2 * palm_voxel_size.x;
        palm_voxel_min_loc.y = - palm_voxel_dim.y / 2 * palm_voxel_size.y;
        palm_voxel_min_loc.z = - palm_voxel_dim.z / 2 * palm_voxel_size.z;
    }
    for(iter_point = trans_cloud->points.begin(); iter_point < trans_cloud->points.end(); iter_point++)
    {
        if (std::isnan(iter_point->x) || std::isnan(iter_point->y) || std::isnan(iter_point->z))
            continue;
        cv::Point3i voxel_loc; 
        voxel_loc.x = int((iter_point->x - palm_voxel_min_loc.x) / palm_voxel_size.x);
        voxel_loc.y = int((iter_point->y - palm_voxel_min_loc.y) / palm_voxel_size.y);
        voxel_loc.z = int((iter_point->z - palm_voxel_min_loc.z) / palm_voxel_size.z);
        if(voxel_loc.x >= 0 && voxel_loc.x < palm_voxel_dim.x &&
           voxel_loc.y >= 0 && voxel_loc.y < palm_voxel_dim.y &&
           voxel_loc.z >= 0 && voxel_loc.z < palm_voxel_dim.z)
            if(palm_voxel_grid_bi[voxel_loc.x][voxel_loc.y][voxel_loc.z] != 1)
            {
                palm_voxel_grid_bi[voxel_loc.x][voxel_loc.y][voxel_loc.z] = 1;
                palm_voxel_grid.push_back(voxel_loc);
            }
    }
  
    return palm_voxel_grid; 
}

GenVoxelFromPcd::voxel_grid GenVoxelFromPcd::genVoxelFromPcd(const std::string &pcd_file,
                                    const cv::Point3i &palm_voxel_dim,
                                    const cv::Point3f &palm_voxel_size)
{
    bool palm_frame = false;
    std::cout << pcd_file << std::endl;
    PointCloudXYZRGB::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::io::loadPCDFile(pcd_file, *cloud);
    cloud->header.frame_id = "blensor_camera";

    PointCloudXYZRGB::Ptr trans_cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    tf::TransformListener listener;
   
    if(palm_frame)
    {
        listener.waitForTransform("grasp_palm_pose", "blensor_camera", ros::Time(0), ros::Duration(10.0) );
        pcl_ros::transformPointCloud("grasp_palm_pose", *cloud, *trans_cloud, listener);
        trans_cloud->header.frame_id = "grasp_palm_pose";
        // Publish pointclouds for debugging
        //publishPointcloud(*cloud, "cloud", *trans_cloud, "cloud_palm");
    }
    else
    {
        listener.waitForTransform("object_pose", "blensor_camera", ros::Time(0), ros::Duration(10.0) );
        pcl_ros::transformPointCloud("object_pose", *cloud, *trans_cloud, listener);
        trans_cloud->header.frame_id = "object_pose";
        //publishPointcloud(*cloud, "cloud", *trans_cloud, "cloud_object");
    }

    voxel_grid palm_voxel_grid;
    std::vector<std::vector<std::vector<int> > > palm_voxel_grid_bi(palm_voxel_dim.x, 
                                                std::vector<std::vector<int> >(palm_voxel_dim.y, 
                                                std::vector<int>(palm_voxel_dim.z, 0)));
    PointCloudXYZRGB::iterator iter_point;

    cv::Point3d palm_voxel_min_loc;
    if(palm_frame)
    {
        palm_voxel_min_loc.x = 0.;
        palm_voxel_min_loc.y = - palm_voxel_dim.y / 2 * palm_voxel_size.y;
        palm_voxel_min_loc.z = - palm_voxel_dim.z / 2 * palm_voxel_size.z;
    }
    else
    {
        palm_voxel_min_loc.x = - palm_voxel_dim.x / 2 * palm_voxel_size.x;
        palm_voxel_min_loc.y = - palm_voxel_dim.y / 2 * palm_voxel_size.y;
        palm_voxel_min_loc.z = - palm_voxel_dim.z / 2 * palm_voxel_size.z;
    }
    for(iter_point = trans_cloud->points.begin(); iter_point < trans_cloud->points.end(); iter_point++)
    {
        if (std::isnan(iter_point->x) || std::isnan(iter_point->y) || std::isnan(iter_point->z))
            continue;
        cv::Point3i voxel_loc; 
        voxel_loc.x = int((iter_point->x - palm_voxel_min_loc.x) / palm_voxel_size.x);
        voxel_loc.y = int((iter_point->y - palm_voxel_min_loc.y) / palm_voxel_size.y);
        voxel_loc.z = int((iter_point->z - palm_voxel_min_loc.z) / palm_voxel_size.z);
        if(voxel_loc.x >= 0 && voxel_loc.x < palm_voxel_dim.x &&
           voxel_loc.y >= 0 && voxel_loc.y < palm_voxel_dim.y &&
           voxel_loc.z >= 0 && voxel_loc.z < palm_voxel_dim.z)
            if(palm_voxel_grid_bi[voxel_loc.x][voxel_loc.y][voxel_loc.z] != 1)
            {
                palm_voxel_grid_bi[voxel_loc.x][voxel_loc.y][voxel_loc.z] = 1;
                palm_voxel_grid.push_back(voxel_loc);
            }
    }
  
    return palm_voxel_grid; 
}
bool GenVoxelFromPcd::genVoxel(grasp_type_inference::GenGraspVoxel::Request& req,
                grasp_type_inference::GenGraspVoxel::Response& res)
{
    cv::Point3i palm_voxel_dim(req.voxel_dim[0], req.voxel_dim[1], req.voxel_dim[2]);
    cv::Point3f palm_voxel_size(req.voxel_size[0], req.voxel_size[1], req.voxel_size[2]);
    //voxel_grid palm_voxel_grid = genVoxelFromPcd(req.pcd_file_path, req.palm_pose, 
    //        palm_voxel_dim, palm_voxel_size);
    //voxel_grid palm_voxel_grid = genVoxelFromPcd(req.pcd_file_path, req.trans_camera_to_palm, 
    //        palm_voxel_dim, palm_voxel_size);
    voxel_grid palm_voxel_grid = genVoxelFromPcd(req.pcd_file_path, palm_voxel_dim, palm_voxel_size);

    unsigned int voxels_num = palm_voxel_grid.size();
    std::vector<int> palm_voxel_grid_1d(3 * voxels_num, -1); 
    // Convert voxel_grid to 1d array.
    for (unsigned int i = 0; i < voxels_num; ++i)
    {
        palm_voxel_grid_1d[3 * i]  = palm_voxel_grid[i].x; 
        palm_voxel_grid_1d[3 * i + 1]  = palm_voxel_grid[i].y; 
        palm_voxel_grid_1d[3 * i + 2]  = palm_voxel_grid[i].z; 
    }
    std::cout << "filled_voxel_num: " << voxels_num << std::endl;
    res.voxel_grid = palm_voxel_grid_1d;
    return true;
}

void GenVoxelFromPcd::publishPointcloud(const PointCloudXYZRGB &pointcloud, 
                                        const std::string &topic_name)
{
    ros::NodeHandle nh;
    uint32_t queue_size = 1; 
    sensor_msgs::PointCloud2 cloud_msg;
    pcl::toROSMsg(pointcloud, cloud_msg);
    ros::Publisher pub = nh.advertise<sensor_msgs::PointCloud2>(topic_name, queue_size);
    ros::Rate loop_rate(10);
    while (ros::ok())
    //for(int i=0; i < 10; ++i)
    {
        pub.publish(cloud_msg);
        ros::spinOnce();
        loop_rate.sleep();
    }
}

void GenVoxelFromPcd::publishPointcloud(const PointCloudXYZRGB &pointcloud, 
                                        const std::string &topic_name, 
                                        const PointCloudXYZRGB &pointcloud_2, 
                                        const std::string &topic_2_name)
{
    ros::NodeHandle nh;
    uint32_t queue_size = 1; 
    sensor_msgs::PointCloud2 cloud_msg;
    pcl::toROSMsg(pointcloud, cloud_msg);
    ros::Publisher pub = nh.advertise<sensor_msgs::PointCloud2>(topic_name, queue_size);
    sensor_msgs::PointCloud2 cloud_msg_2;
    pcl::toROSMsg(pointcloud_2, cloud_msg_2);
    ros::Publisher pub2 = nh.advertise<sensor_msgs::PointCloud2>(topic_2_name, queue_size);
    ros::Rate loop_rate(10);
    //while (ros::ok())
    for(int i = 0; i < 20; ++i)
    {
        pub.publish(cloud_msg);
        pub2.publish(cloud_msg_2);
        ros::spinOnce();
        loop_rate.sleep();
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "gen_voxel_server");
    ros::NodeHandle n;

    GenVoxelFromPcd gen_voxel_from_pcd;    
    ros::ServiceServer service = n.advertiseService("/gen_voxel_from_pcd", &GenVoxelFromPcd::genVoxel, 
                                                    &gen_voxel_from_pcd);
    ROS_INFO("Service gen_voxel_from_pcd:");
    ROS_INFO("Ready to generate voxel from pcd.");

    ros::spin();

    return 0;
}

