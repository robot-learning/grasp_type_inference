cmake_minimum_required(VERSION 2.8.3)
project(grasp_type_inference)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  grasp_cnn_inference
  message_generation
  pcl_conversions
  pcl_ros
  cv_bridge
)

find_package(PCL REQUIRED)
find_package(OpenCV REQUIRED)
catkin_python_setup()

## Generate messages in the 'msg' folder
# add_message_files(
#   FILES
#   Message1.msg
#   Message2.msg
# )

## Generate services in the 'srv' folder
 add_service_files(
   FILES
   GenGraspVoxel.srv
   UpdatePalmPose.srv
   UpdatePalmPosesInObj.srv
   UpdatePriorPoses.srv
   UpdateObjectPose.srv
   GenInfVoxel.srv
   GraspPgmInf.srv
 )


## Generate added messages and services with any dependencies listed here
 generate_messages(
   DEPENDENCIES
   grasp_cnn_inference
   std_msgs
   sensor_msgs
   geometry_msgs
 )

catkin_package(
  CATKIN_DEPENDS
  message_runtime
  roscpp
  rospy
  std_msgs
  pcl_conversions
  DEPENDS
  pcl_ros
  cv_bridge
  pcl
  opencv
)

include_directories(
  ${catkin_INCLUDE_DIRS}
  ${CMAKE_CURRENT_SOURCE_DIR}/include
)

add_executable(gen_voxel_from_pcd src/gen_voxel_from_pcd.cpp)
add_dependencies(gen_voxel_from_pcd grasp_type_inference_generate_messages_cpp)
target_link_libraries(gen_voxel_from_pcd ${catkin_LIBRARIES} ${OpenCV_LIBRARIES} ${PCL_LIBRARIES})

add_executable(gen_inference_voxel src/gen_inference_voxel.cpp)
add_dependencies(gen_inference_voxel grasp_type_inference_generate_messages_cpp)
target_link_libraries(gen_inference_voxel ${catkin_LIBRARIES} ${OpenCV_LIBRARIES} ${PCL_LIBRARIES})

# add_executable(test_obj_segmentation src/test_obj_segmentation.cpp)
# add_dependencies(test_obj_segmentation grasp_type_inference_generate_messages_cpp)
# target_link_libraries(test_obj_segmentation ${catkin_LIBRARIES} ${OpenCV_LIBRARIES} ${PCL_LIBRARIES})
