### What is this repository for? ###
Learning and inference code for the grasp model modeling grasp types.  

### Learning and Inference ###
Learning: grasp_pgm_learner.py   
Inference: grasp_pgm_inference.py    
Inference ROS server: grasp_pgm_inf_server.py    

### How do I generate voxel grids from pointcloud? ###
roscore    
rosrun grasp_type_inference gen_voxel_from_pcd    
rosrun grasp_type_inference broadcast_palm_tf.py    
rosrun grasp_data_collection_pkg broadcast_blensor_camera_tf.py    
rosrun grasp_type_inference proc_grasp_data.py    
